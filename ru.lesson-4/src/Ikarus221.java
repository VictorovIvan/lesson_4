import batteries.Battery;
import bepeers.Beeper;
import bodies.Body;
import bumpers.Bumper;
import cabins.Сabin;
import colors.Color;
import engines.Engine;
import glasses.Glass;
import interiors.Interior;
import tanks.Tank;
import tires.Tire;

public class Ikarus221 extends Bus{
    public Ikarus221(Battery battery, Beeper beeper, Body body, Bumper bumper, Color color, Engine engine, Glass glass, Interior interior, Tank tank, Tire tire, Сabin cabin) {
        super(battery, beeper, body, bumper, color, engine, glass, interior, tank, tire, cabin);
    }

    public void scoopFuel() {
        System.out.println("Fuel is scoped");
    }

    public void landToPlanet() {
        System.out.println("Vehicle is landed");
    }
}
