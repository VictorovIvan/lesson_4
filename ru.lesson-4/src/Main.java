import batteries.Akom;
import batteries.Battery;
import batteries.Panasonik;
import batteries.Varta;
import bepeers.Beeper;
import bepeers.FancyMusic;
import bepeers.Horn;
import bepeers.MagicSparks;
import bodies.BigBody;
import bodies.Body;
import bodies.MiddleBody;
import bodies.SmallBody;
import bumpers.BigBumper;
import bumpers.Bumper;
import bumpers.MiddleBumper;
import bumpers.SmallBumper;
import cabins.BigCabin;
import cabins.MiddleCabin;
import cabins.SmallCabin;
import cabins.Сabin;
import colors.Black;
import colors.Color;
import colors.Red;
import colors.White;
import engines.Engine;
import engines.Engine12V;
import engines.Engine20V;
import glasses.ClearGlass;
import glasses.Glass;
import interiors.Convenience;
import interiors.Interior;
import tanks.BigTank;
import tanks.MiddleTank;
import tanks.Tank;
import tires.Tire;
import tires.Wheel14;
import tires.Wheel25;
import trunks.BigTrunk;
import trunks.MiddleTrunk;
import trunks.Trunk;


public class Main {

    public static void main(String[] args) {
        Battery akom = new Akom();
        Battery panasonik = new Panasonik();
        Battery varta = new Varta();
        Beeper fancyMusic = new FancyMusic();
        Beeper horn = new Horn();
        Beeper magicSparks = new MagicSparks();
        Body bigBody = new BigBody();
        Body middleBody = new MiddleBody();
        Body smallBody = new SmallBody();
        Bumper bigBumper = new BigBumper();
        Bumper middleBumper = new MiddleBumper();
        Bumper smallBumper = new SmallBumper();
        Сabin smallCabin = new SmallCabin();
        Сabin middleCabin = new MiddleCabin();
        Сabin bigCabin = new BigCabin();
        Color black = new Black();
        Color red = new Red();
        Color white = new White();
        Engine engine12V = new Engine12V();
        Engine engine20V = new Engine20V();
        Glass clearGlass = new ClearGlass();
        Interior convenince = new Convenience();
        Tank middleTank = new MiddleTank();
        Tank bigTank = new BigTank();
        Tire wheel14 = new Wheel14();
        Tire wheel25 = new Wheel25();
        Trunk bigTrunk = new BigTrunk();
        Trunk middleTrunk = new MiddleTrunk();
        Lambo lambo = new Lambo(panasonik, fancyMusic, smallBody, middleBumper, black, engine12V,
                clearGlass, convenince, middleTank, wheel14);
        Viper viper = new Viper(panasonik, fancyMusic, smallBody, middleBumper, red, engine12V,
                clearGlass, convenince, middleTank, wheel14);
        Lada2199 lada2199 = new Lada2199(akom, horn, smallBody, middleBumper, red, engine12V,
                clearGlass, convenince, middleTank, wheel14);
        Fiat2011 fiat2011 = new Fiat2011(akom, fancyMusic, smallBody, middleBumper, red, engine12V,
                clearGlass, convenince, middleTank, wheel14);
        Kamaz221 kamaz221 = new Kamaz221(akom, fancyMusic, bigBody, bigBumper, white, engine20V,
                clearGlass, convenince, bigTank, wheel25, bigTrunk);
        Ikarus221 ikarus221 = new Ikarus221(akom, fancyMusic, bigBody, bigBumper, red, engine20V,
                clearGlass, convenince, bigTank, wheel25,bigCabin);


        System.out.println("Lambo:" + lambo.toString());
        System.out.println("Viper:" + viper.toString());
        System.out.println("Lada2199:" + lada2199.toString());
        System.out.println("Fiat2011:" + fiat2011.toString());
        System.out.println("Kamaz221:" + kamaz221.toString());
        System.out.println("Ikarus221:" + ikarus221.toString());


    }
}
