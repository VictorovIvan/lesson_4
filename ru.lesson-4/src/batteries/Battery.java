package batteries;

public interface Battery {
    public String makeEnergy();
}
