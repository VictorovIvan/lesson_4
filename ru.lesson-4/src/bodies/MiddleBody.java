package bodies;

public class MiddleBody implements  Body {

    @Override
    public String makeSeeBody() {
        return "It's middle body";
    }
}
