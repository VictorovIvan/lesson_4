package trunks;

public class MiddleTrunk implements Trunk{
    @Override
    public String makeSeeTrunk() {
        return "It's middle trunk";
    }
}
