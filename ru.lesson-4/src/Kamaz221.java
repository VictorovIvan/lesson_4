import batteries.Battery;
import bepeers.Beeper;
import bodies.Body;
import bumpers.Bumper;
import colors.Color;
import engines.Engine;
import glasses.Glass;
import interiors.Interior;
import tanks.Tank;
import tires.Tire;
import trunks.Trunk;

public class Kamaz221 extends Truck{
    public Kamaz221(Battery battery, Beeper beeper, Body body, Bumper bumper, Color color, Engine engine, Glass glass, Interior interior, Tank tank, Tire tire, Trunk trunk) {
        super(battery, beeper, body, bumper, color, engine, glass, interior, tank, tire, trunk);
    }

    public void scoopFuel() {
        System.out.println("Fuel scop");
    }

    public void landToPlanet() {
        System.out.println("Vehicle");
    }
}
