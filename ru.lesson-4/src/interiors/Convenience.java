package interiors;

public class Convenience implements Interior{
    @Override
    public String makeMachineComfort() {
        return "It's convenience interior";
    }
}
