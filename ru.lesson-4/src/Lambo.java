import batteries.Battery;
import bepeers.Beeper;
import bodies.Body;
import bumpers.Bumper;
import colors.Color;
import engines.Engine;
import glasses.Glass;
import interiors.Interior;
import tanks.Tank;
import tires.Tire;

public class Lambo  extends Car {
    public Lambo(Battery battery, Beeper beeper, Body body, Bumper bumper, Color color, Engine engine, Glass glass, Interior interior, Tank tank, Tire tire) {
        super(battery, beeper, body, bumper, color, engine, glass, interior, tank, tire);
    }

    public void scoopFuel() {
        System.out.println("Fuel scoped");
    }

    public void landToPlanet() {
        System.out.println("Vehicle landed");
    }
}
