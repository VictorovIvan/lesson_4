import batteries.Battery;
import bepeers.Beeper;
import bodies.Body;
import bumpers.Bumper;
import colors.Color;
import engines.Engine;
import glasses.Glass;
import interiors.Interior;
import tanks.Tank;
import tires.Tire;
import trunks.Trunk;

public abstract class Truck {
    private Battery battery;
    private Beeper beeper;
    private Body body;
    private Bumper bumper;
    private Color color;
    private Engine engine;
    private Glass glass;
    private Interior interior;
    private Tank tank;
    private Tire tire;
    private Trunk trunk;

    public Truck(Battery battery, Beeper beeper, Body body, Bumper bumper,
               Color color, Engine engine, Glass glass, Interior interior,
               Tank tank, Tire tire, Trunk trunk) {
        this.battery = battery;
        this.beeper = beeper;
        this.body = body;
        this.bumper = bumper;
        this.color = color;
        this.engine = engine;
        this.glass = glass;
        this.interior = interior;
        this.tank = tank;
        this.tire = tire;
        this.trunk = trunk;
    }

    public String checkEnergy() {
        return battery.makeEnergy();
    }

    public String checkBeep() {
        return beeper.makeSound();
    }

    public String  checkAllColor() {
        return body.makeSeeBody();
    }

    public String checkForwardSize() {
        return bumper.makeSeeBumper();
    }

    public String checkColor() {
        return color.makeSeeColor();
    }

    public String checkSpeed() {
        return engine.makeRotation();
    }

    public String checkTransparency() {
        return glass.makeSeeGlass();
    }

    public String checkСonvenience() {
        return interior.makeMachineComfort();
    }

    public String checkVolumeGasTank() {
        return tank.makeFillTheTank();
    }

    public String checkRoadTraction() {
        return tire.makeSpinWheels();
    }

    public String makeOnTrunk() {
        return trunk.makeSeeTrunk();
    }

    @Override
    public String toString() {
        return "battery=" + battery.makeEnergy() +
                ", beeper=" + beeper.makeSound() +
                ", body=" + body.makeSeeBody() +
                ", bumper=" + bumper.makeSeeBumper() +
                ", color=" + color.makeSeeColor() +
                ", engine=" + engine.makeRotation() +
                ", glass=" + glass.makeSeeGlass() +
                ", interior=" + interior.makeMachineComfort() +
                ", tank=" + tank.makeFillTheTank() +
                ", tire=" + tire.makeSpinWheels() +
                ", trunk=" + trunk.makeSeeTrunk();
    }
}
