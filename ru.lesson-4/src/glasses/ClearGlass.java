package glasses;

public class ClearGlass implements Glass{
    @Override
    public String makeSeeGlass() {
        return "It's clear glass";
    }
}
