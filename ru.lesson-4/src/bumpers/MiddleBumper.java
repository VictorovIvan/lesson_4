package bumpers;

public class MiddleBumper implements Bumper {
    @Override
    public String makeSeeBumper() {
        return "It's middle bumper";
    }
}
