package bepeers;

public interface Beeper {
    public String makeSound();
}
