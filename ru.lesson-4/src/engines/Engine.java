package engines;

public interface Engine {
    public String makeRotation();
}
